package controller;

import java.io.Serializable;

import nutsAndBolts.PieceSquareColor;

/**
 * @author francoise.perrin
 *
 * Objet � destination de la View
 * cr�� par le Controller
 * � partir des donn�es retourn�es par le Model
 * 
 */
public class InputViewData<T> implements Serializable{
	
	private static final long serialVersionUID = 1L;

	public T toMovePieceIndex = null;
	public T targetSquareIndex = null;
	public T capturedPieceIndex = null;
	public T dPieceIndex = null;
	public PieceSquareColor dPieceColor = null;
	
	
	public InputViewData(
			T toMovePieceIndex, 
			T targetSquareIndex, 
			T capturedPieceIndex,
			T dPieceIndex,
			PieceSquareColor dPieceColor) {
		super();
		this.toMovePieceIndex = toMovePieceIndex;
		this.targetSquareIndex = targetSquareIndex;
		this.capturedPieceIndex = capturedPieceIndex;
		this.dPieceIndex = dPieceIndex;
		this.dPieceColor = dPieceColor;
	}


	@Override
	public String toString() {
		return "DataAfterMove [toMovePieceIndex=" + toMovePieceIndex
				+ ", targetSquareIndex=" + targetSquareIndex + ", capturedPieceIndex=" + capturedPieceIndex
				+ ", dPieceIndex=" + dPieceIndex + ", dPieceColor=" + dPieceColor + "]";
	}


	

	
}
